Explanation:
#Parent Class
In this program, The base class should be Vehicle, with subclasses Truck, Car and Electric car, 
i first define a parent abstract class called Vehicle with the following methods, 
Each subclass can have common properties such as make, model etc and implement methods below which are obvious in any Vehicle.
* drive(double driving_distance) that will be used to drive any vehicle
* fillUpPetrol(double fillUpAmount,double costPerLiter) fill up petrol on any vehicle i need to know the topup amount and the cost per liter of petrol
* changeOil() that returns remaining days due for changing of Oil in the vehicle and if the days remaining is <=0 then Vehicle is due for oil change
* printStats() to print the stats of the Vehicle

#Children/Sub-Classes

#Car
* The Car class is a child class of Vehicle and extends the Vehicle class. It has an additional instance variables, liters,oilChangeRemdays,current_date,set_next_oil_change_date. 
* The class has a constructor that accepts all the necessary parameters. 
* The class overrides the four(4) abstract methods of the parent class and provides specific implementations of the methods.

#Truck
* The Truck class is a child class of Vehicle and extends the Vehicle class. It has an additional instance variable, cargoSpace. 
* The class has a constructor that accepts all the necessary parameters including cargo Space. 
* The class overrides the four(4) abstract methods of the parent class and provides specific implementations of the methods.

#ElectricCar
* The Truck class is a child class of Vehicle and extends the Vehicle class,It may have additional instance variables specific to Electric cars
* There is no method implementation in the [ElectricCar class]
